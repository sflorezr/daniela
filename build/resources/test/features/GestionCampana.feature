# language: es
  #Autor: sflorez@solati.com.co

Característica: Pruebas creacion campañas, agentes virtuales


  Esquema del escenario: Pruebas funcionales a pantalla crear campaña
    Dado Ingreso al aplicativo adminfo
    Cuando Ingreso al modulo de mantenimiento en la opcion gestion campana
    Y Digito los datos de informacion general nombre:"<nombre>" fecha inicial:"<fecha_inicial>" fecha final:"<fecha_final>" plazo: "<plazo>" envio a:"<envio_a>" tipo envio:"<tipo_envio>" entidad:"<entidad>" tipo campana:"<tipo_campana>" codig_exitoso:"<codig_exitoso>" codigo fallido:"<codig_fallido>" hora inicio:"<horainicio>" estado:"<estado>" carga desde:"<carga_desde>" flash:"<flash>"
    Entonces Valido respuesta de pantalla informacion general "<mensaje_esperado>"
    Cuando Digito criterios de busqueda "<mensaje_esperado>"
      | Columna    | operador  | valor   | mensaje |
      | Saldo Neto | Mayor Que | 1000000 |         |
    Y Selecciono plantilla "<plantilla>" Y Digito numero o correo oculto y guardo campaña "<oculto>"
    Entonces Valido ultimo mensaje "<ultimomensaje>" con nombre "<nombre>"

    Ejemplos:
      | nombre      | fecha_inicial | fecha_final | plazo | envio_a | tipo_envio | entidad | tipo_campana | codig_exitoso                 | codig_fallido | horainicio | estado | carga_desde | Flash | mensaje_esperado | plantilla   | oculto     | ultimomensaje                |
      | Prueba_SMS1 | 2020-03-27    | 2020-03-31  | 10    | Deudor  | Nit        | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000 | guardada satisfactoriamente. |
    #  | Prueba_SMS1.1   | 2020-03-27    | 2020-03-31  | 10    | Deudor            | IDPV       | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL   |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000            | guardada satisfactoriamente. |
    #  | Prueba_SMS2     | 2020-03-27    | 2020-03-31  | 10    | Codeudor          | Nit        | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL   |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000            | guardada satisfactoriamente. |
    #  | Prueba_SMS2.1   | 2020-03-27    | 2020-03-31  | 10    | Codeudor          | IDPV       | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL   |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000            | guardada satisfactoriamente. |
    #  | Prueba_SMS3     | 2020-03-27    | 2020-03-31  | 10    | Deudor + Codeudor | Nit        | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL   |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000            | guardada satisfactoriamente. |
    #  | Prueba_SMS3.1   | 2020-03-27    | 2020-03-31  | 10    | Deudor + Codeudor | IDPV       | CONFIAR | SMS          | SMS GESTION DE AGENTE VIRTUAL   |               | 07:15      | Activa | No          | No    |                  | PruebaRobot | 3000000000            | guardada satisfactoriamente. |
    #  | Prueba_Email1   | 2020-03-27    | 2020-03-31  | 10    | Deudor            | Nit        | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |
    #  | Prueba_Email1.1 | 2020-03-27    | 2020-03-31  | 10    | Deudor            | IDPV       | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |
    #  | Prueba_Email2   | 2020-03-27    | 2020-03-31  | 10    | Codeudor          | Nit        | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |
    #  | Prueba_Email2.1 | 2020-03-27    | 2020-03-31  | 10    | Codeudor          | IDPV       | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |
    #  | Prueba_Email3   | 2020-03-27    | 2020-03-31  | 10    | Deudor + Codeudor | Nit        | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |
    #  | Prueba_Email3.1 | 2020-03-27    | 2020-03-31  | 10    | Deudor + Codeudor | IDPV       | CONFIAR | EMAIL        | EMAIL GESTION DE AGENTE VIRTUAL |               | 07:15      | Activa | No          | No    |                  | MaryProrrogasQA     | sflorez@solati.com.co | guardada satisfactoriamente. |