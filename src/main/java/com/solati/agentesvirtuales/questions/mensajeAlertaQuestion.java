package com.solati.agentesvirtuales.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.JavaScriptClick;

import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.BOTON_SIGUIENTE;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class mensajeAlertaQuestion implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
       /* theActorInTheSpotlight().attemptsTo(JavaScriptClick.on(BOTON_SIGUIENTE));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
       System.out.println((String) actor.recall("ULTIMOMENSAJE"));
        return actor.recall("ULTIMOMENSAJE");
    }
    public static Question<String> mensaje(){return new mensajeAlertaQuestion();  }
}
