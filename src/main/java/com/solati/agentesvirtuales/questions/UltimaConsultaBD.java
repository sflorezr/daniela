package com.solati.agentesvirtuales.questions;

import com.solati.agentesvirtuales.interactions.Consultar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.sql.SQLException;

public class UltimaConsultaBD implements Question<String> {
    private String campo;

    public UltimaConsultaBD(String campo){
        this.campo=campo;
    }

    @Override
    public String answeredBy(Actor actor) {
        try{
            return Consultar.obtenerUltimaRespuesta().getString(campo).trim();
        } catch (SQLException e) {
            return ("consulta vacia");
        }
    }
    public static UltimaConsultaBD consulta(String campo){
        return new UltimaConsultaBD(campo);
    }
}
