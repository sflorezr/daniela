package com.solati.agentesvirtuales.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ultimaCantidadQuestion implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return actor.recall("cantidadaprobada");
    }
    public static ultimaCantidadQuestion cantidad(){
        return new ultimaCantidadQuestion();
    }
}
