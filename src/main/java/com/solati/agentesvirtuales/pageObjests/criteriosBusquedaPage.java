package com.solati.agentesvirtuales.pageObjests;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class criteriosBusquedaPage extends PageObject {
    public static Target TBL_COLUMNA=Target.the("lista para busqueda de columna").located(By.id("col"));
    public static Target TBL_OPERADOR=Target.the("lista para busqueda de operador").located(By.id("ope"));
    public static Target TXT_CAMPO =Target.the("Caja de texto para el campo valor").located(By.id("valor"));
    public static Target BTN_AGREGAT =Target.the("Boton agregar criterio").located(By.id("btnAgregarCriterio"));
    public static Target BTN_SIGUIENTE = Target.the("Boton acceso").located(net.serenitybdd.core.annotations.findby.By.id("step1Next"));
}
