package com.solati.agentesvirtuales.pageObjests;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

//@DefaultUrl("https://qa.solati.corp/vsmart/")
@DefaultUrl("https://qa.solati.corp/vsmart/")
public class agentesExternosPage extends PageObject {
    public static Target BOTON_CONFIGURACION = Target.the("Boton configuracion avanzada").located(By.id("details-button"));
    public static Target BOTON_ACCEDER = Target.the("Boton acceso").located(By.id("proceed-link"));
    public static Target TXT_USUIARIO = Target.the("Caja de texto usuario").located(By.id("usuario"));
    public static Target TXT_PASSWORD = Target.the("Caja de texto password").located(By.id("clave"));
    public static Target BOTON_ENTRAR = Target.the("Boton entrar").located(By.xpath("//*[@class='botonLogin']"));
    public static Target BOTON_CONTENEDOR = Target.the("Boton contenedor").located(By.xpath("//*[@id='div2_sbs_1045179192168421']"));
    public static Target BOTON_MANTENIMIENTO = Target.the("Boton mantenimiento").locatedBy("//*[@id='irMantenimiento']");
    public static Target BOTON_GESTION_CAMPANA = Target.the("Boton gestion campaña").located(By.xpath("//div[text()='Gestión Campañas']"));
    public static Target BOTON_NUEVA_CAMPANA = Target.the("Boton nueva campaña").located(By.xpath("//*[@id='new-campain' and text()='   NUEVA CAMPAÑA']"));
    public static Target TXT_NOMBRE = Target.the("Caja de texto nombre").located(By.id("nomivr"));
    public static Target TXT_FECHA_INI = Target.the("Caja de texto fecha inicial").located(By.id("fecha_ini"));
    //public static Target DATE_PICKER = Target.the("Caja de texto fecha inicial").located(By.xpath("//*[@id='ui-datepicker-div'][2]"));
    public static Target DATE_PICKER_INI_MES = Target.the("Caja de texto fecha inicial mes").located(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/select[1]"));
    public static Target DATE_PICKER_INI_ANO = Target.the("Caja de texto fecha inicial año").located(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/select[2]"));
    public static Target DATE_PICKER_INI_DIA = Target.the("Caja de texto fecha inicial dia").locatedBy("//*[@id='ui-datepicker-div']/table/tbody/tr[{0}]/td[{1}]");
    public static Target TXT_FECHA_FIN = Target.the("Caja de texto fecha final").located(By.id("fecha_fin"));
    public static Target DATE_PICKER_FIN_MES = Target.the("Caja de texto fecha inicial mes").located(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/select[1]"));
    public static Target DATE_PICKER_FIN_ANO = Target.the("Caja de texto fecha inicial año").located(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/select[2]"));
    public static Target DATE_PICKER_FIN_DIA = Target.the("Caja de texto fecha inicial dia").located(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/select[1]"));
    public static Target TXT_PLAZO = Target.the("Caja de texto dias plazo").located(By.id("diasplazopagos"));
    public static Target TBL_TIPO_ENVIO = Target.the("ComboBox tipo de envio ").located(By.id("tipoagrupador"));
    public static Target TBL_ENTIDAD = Target.the("ComboBox entidad ").located(By.id("concontrol"));
    public static Target TBL_ENVIO_A = Target.the("ComboBox envio a").located(By.id("pordeudorcodeudor"));
    public static Target TBL_TIPO_CAMPANA = Target.the("ComboBox tipo de campaña ").located(By.id("tipocampana"));
    public static Target TBL_COD_GES_EXITO = Target.the("ComboBox codigo de gestion exitoso ").located(By.id("codcobivr"));
    public static Target TBL_COD_GES_FALLIDO = Target.the("ComboBox codigo de gestion fallido ").located(By.id("codcobivrfallido"));
    public static Target TBL_HORA_INICIO = Target.the("ComboBox hora de inicio ").located(By.id("lunes"));
    public static Target CHECK_MARTES = Target.the("Check sin envio martes").located(By.id("dia_sin_envio_martes"));
    public static Target CHECK_MIERCOLES = Target.the("Check sin envio miercoles").located(By.id("dia_sin_envio_miercoles"));
    public static Target CHECK_JUEVES = Target.the("Check sin envio jueves").located(By.id("dia_sin_envio_jueves"));
    public static Target CHECK_VIERNES = Target.the("Check sin envio viernes").located(By.id("dia_sin_envio_viernes"));
    public static Target CHECK_SABADO = Target.the("Check sin envio sabado").located(By.id("dia_sin_envio_sabado"));
    public static Target CHECK_DOMINGO = Target.the("Check sin envio domingo").located(By.id("dia_sin_envio_domingo"));
    public static Target TBL_ESTADO = Target.the("ComboBox estado de la campaña  ").located(By.id("estado"));
    public static Target TBL_CARGA_ARCHIVO = Target.the("ComboBox carga desde archivo (si/no)").located(By.id("desdearchivo"));
    public static Target BOTON_SIGUIENTE = Target.the("Boton acceso").located(By.id("step0Next"));
    public static Target BOTON_CERRAR = Target.the("Boton cerrar").located(By.xpath("//*[@class='cluetip iconoCerrarSesion icon-menu']"));
    public static Target LABEL_MENSAJE = Target.the("Mensaje de error").located(By.xpath("//*[@id='SexyAlertBox-BoxContenedor']/text()"));
    public static Target BTN_GUARDAR = Target.the("Boton guardar").located(By.id("step2End"));
    public static Target TBL_PLANTILLA = Target.the("ComboBox plantillas").located(By.id("consplantilla"));
    public static Target TXT_NUMOCULTO = Target.the("Txt numero oculto").located(By.id("numocultos"));
    public static Target LBL_CANTIDAD_REGISTROS = Target.the("lbl cantidad a enviar ").located(By.id("numRegAEnviarPorCriterios"));
}
