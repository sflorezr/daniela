package com.solati.agentesvirtuales.pageObjests;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class aprobarCampanaPage extends PageObject {
    public static Target BTN_APROBAR_CAMPANA = Target.the("Boton aprobar campaña").located(By.xpath("//div[contains(text(),'Aprobar Campañas')]"));
    public static Target BTN_DETALLE = Target.the("Boton detalles campaña").locatedBy("//td[text()='{0}']/../td[5]/input");
    public static Target LBL_NUMERO_REGISTROS = Target.the("Label cantidad de registros").located(By.id("countCampanaConsultada"));
    public static Target BTN_APROBAR_CAMPANAS = Target.the("Boton aprobar campañas").located(By.xpath("//*[@id='divVentanaDetalleCampana']/input[2]"));
    public static Target BTN_MENU_AGENTES = Target.the("menu agentes virtuales").locatedBy("//span[text()='Agentes Virtuales']");
    public static Target BTN_SUB_MENU_GESTION_CAMPANA = Target.the("Sub menu gestion campañas").locatedBy("//a[@title='Gestión Campañas']");
    public static Target BTN_ENVIAR = Target.the("Enviar campaña").locatedBy("//div[@title='{0}']/../../div[2]/a[3]");
}
