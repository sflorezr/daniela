package com.solati.agentesvirtuales.abilities;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.solati.agentesvirtuales.utils.exceptions.ActorNoPuedeConsultarLaBD;
import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;
import org.apache.log4j.Logger;

public class ConsultaAbaseDatosMySQL implements Ability {
    private static final Logger LOGGER = Logger.getLogger(ConsultaAbaseDatosMySQL.class.getName());

    public ConsultaAbaseDatosMySQL() {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession("sflorez","10.10.8.2",22);
            session.setPassword("Nakia*310");
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
            System.out.println("si se pudo conectar");
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException | JSchException e) {
            LOGGER.error(e.getMessage());
            throw new BackendException(
                    "com.mysql.jdbc.Driver, debes cargar el jar MySQL en el proyecto");
        }
    }
    public static ConsultaAbaseDatosMySQL mySQl(){
        return new ConsultaAbaseDatosMySQL();
    }

    public static ConsultaAbaseDatosMySQL as (Actor actor){
        if(actor.abilityTo(ConsultaAbaseDatosMySQL.class)== null ){
            throw new ActorNoPuedeConsultarLaBD(actor.getName());
        }
        return actor.abilityTo(ConsultaAbaseDatosMySQL.class);
    }
    @Override
    public String toString() {
        return "Consulta base de datos";
    }
}
