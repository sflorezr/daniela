package com.solati.agentesvirtuales.abilities;

import com.solati.agentesvirtuales.utils.exceptions.ActorNoPuedeConsultarLaBD;
import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import io.vavr.collection.List;
import net.serenitybdd.screenplay.Ability;

import net.serenitybdd.screenplay.Actor;
import org.apache.log4j.Logger;

public class ConsultaAbaseDatos implements Ability {
    private static final Logger LOGGER = Logger.getLogger(ConsultaAbaseDatos.class.getName());

    public ConsultaAbaseDatos() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage());
            throw new BackendException(
                    "org.postgresql.Driver, debes cargar el jar postgres en el proyecto");
        }
    }
    public static ConsultaAbaseDatos postGres(){
        return new ConsultaAbaseDatos();
    }

    public static ConsultaAbaseDatos as (Actor actor){
        if(actor.abilityTo(ConsultaAbaseDatos.class)== null ){
            throw new ActorNoPuedeConsultarLaBD(actor.getName());
        }
        return actor.abilityTo(ConsultaAbaseDatos.class);
    }
    @Override
    public String toString() {
        return "Consulta base de datos";
    }
}
