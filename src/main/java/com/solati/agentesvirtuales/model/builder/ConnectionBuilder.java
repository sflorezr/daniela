package com.solati.agentesvirtuales.model.builder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.solati.agentesvirtuales.utils.SSHConnector;
import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import org.apache.log4j.Logger;

public class ConnectionBuilder {
    private static final Logger LOGGER = Logger.getLogger(ConnectionBuilder.class.getName());

    private String user;
    private String password;
    private String url;

    public ConnectionBuilder(String url) {
        this.url = url;

    }

    public static ConnectionBuilder crearNuevaConexion(String url) {
        return new ConnectionBuilder(url);
    }

    public ConnectionBuilder conUsuario(String user) {
        this.user = user;
        return this;
    }

    public ConnectionBuilder yClave(String password) {
        this.password = password;
        return this;
    }

    public Connection build() {
        Connection connection=null;
        try {
            /*SSHConnector sshConnector = new SSHConnector();
            sshConnector.connect("sflorez","Nakia*310","10.10.8.2",22);*/
            connection = DriverManager.getConnection(url, user, password);
            LOGGER.info("conexion exitosa a la base de datos");
            return connection;
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new BackendException(
                    "no se pudo realizar la conexion a " + url + " con usuario:" + user + " y password:" + password);
        }

    }
}
