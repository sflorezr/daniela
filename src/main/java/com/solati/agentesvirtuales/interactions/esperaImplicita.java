package com.solati.agentesvirtuales.interactions;

import net.serenitybdd.markers.IsSilent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class esperaImplicita implements Interaction, IsSilent {
    private Integer segundos;
    public esperaImplicita(Integer segundos){
        this.segundos=segundos;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            Thread.sleep(segundos);
            System.out.println("si espero nada");
        } catch (InterruptedException e) {
            System.out.println("no espero nada");
            e.printStackTrace();
        }
    }
    public static esperaImplicita por(Integer segundos){
        return new esperaImplicita(segundos);
    }
}
