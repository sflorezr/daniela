package com.solati.agentesvirtuales.interactions;

import com.solati.agentesvirtuales.utils.TomaEvidencia;
import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.thucydides.core.annotations.Step;

import java.sql.*;
import java.util.List;
import org.apache.log4j.Logger;

import static java.util.Arrays.asList;

public class Consultar implements Interaction {
    private static final Logger LOGGER = Logger.getLogger(Consultar.class.getName());
    private String query;
    private String Cadena="";
    private List<Object> parametros;
    public static ResultSet ultimaConsultaBD;

    public Consultar(String query){
        this.query=query;
    }
    public Consultar conParametros(Object... parametros){
        this.parametros=asList(parametros);
        return this;
    }

    @Step("{1} Consulta desde Postgres")
    @Override
    public <T extends Actor> void performAs(T actor) {
        final Connection connection = actor.recall("ConexionBDpostgres");
        int indexPrepareStatementr = 1;
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for(Object parametro : parametros){
                preparedStatement.setObject(indexPrepareStatementr, parametro);
                indexPrepareStatementr++;
            }
            ultimaConsultaBD=preparedStatement.executeQuery();
            TomaEvidencia.deBaseDeDatos(ultimaConsultaBD);
            indexPrepareStatementr = 1;
            for(Object parametro : parametros){
                preparedStatement.setObject(indexPrepareStatementr, parametro);
                indexPrepareStatementr++;
            }
            ultimaConsultaBD=preparedStatement.executeQuery();
        }catch (SQLException e){
            LOGGER.error(e.getMessage());
            throw new BackendException("no se pudo preparar el query para Postgres");
        }
    }
    public static Consultar conQuery(String query){
        return new Consultar(query);
    }

    public static ResultSet obtenerUltimaRespuesta(){
        return ultimaConsultaBD;
    }
}
