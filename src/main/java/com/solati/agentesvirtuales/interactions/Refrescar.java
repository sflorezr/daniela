package com.solati.agentesvirtuales.interactions;

import net.serenitybdd.markers.IsSilent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class Refrescar implements Interaction, IsSilent {
    @Override
    public <T extends Actor> void performAs(T actor) {
        BrowseTheWeb.as(actor).getDriver().manage().deleteAllCookies();
        BrowseTheWeb.as(actor).getDriver().navigate().refresh();
    }

    public static Refrescar navegadorActivo() {
        return new Refrescar();
    }
}
