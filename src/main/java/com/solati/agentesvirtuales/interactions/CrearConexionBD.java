package com.solati.agentesvirtuales.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Step;

import static com.solati.agentesvirtuales.model.builder.ConnectionBuilder.crearNuevaConexion;

public class CrearConexionBD implements Interaction {

    private String userName;
    private String password;
    private String url;

    public CrearConexionBD(String url) {
        this.url = url;
    }

    public CrearConexionBD conUsuario(String userName) {
        this.userName = userName;
        return this;
    }

    public CrearConexionBD yClave(String password) {
        this.password = password;
        return this;
    }

    public static CrearConexionBD aBaseDeDatos(String url) {
        return Tasks.instrumented(CrearConexionBD.class, url);
    }

    @Step("{0} crea nueva conexion a #url con usuario:#userName y clave:#password")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.remember("ConexionBDpostgres",crearNuevaConexion(url).conUsuario(userName).yClave(password).build());
    }
}
