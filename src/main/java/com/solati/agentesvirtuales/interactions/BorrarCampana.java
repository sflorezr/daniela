package com.solati.agentesvirtuales.interactions;

import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BorrarCampana implements Interaction {
    private static final Logger LOGGER = Logger.getLogger(BorrarCampana.class.getName());
    private String campana;

    public BorrarCampana(String campana){
        this.campana=campana;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        final Connection connection = actor.recall("ConexionBDpostgres");
        try{
            PreparedStatement preparedStatement = connection.prepareStatement("delete from criterios_datos where nombre =(select md5(consivr) from administracionivr where nomivr=?)");
            preparedStatement.setObject(1, campana);
            preparedStatement.executeUpdate();
            LOGGER.info("BORRO CRITERIOS");
            preparedStatement = connection.prepareStatement("delete from administracionivr where nomivr=?");
            preparedStatement.setObject(1, campana);
            preparedStatement.executeUpdate();
            LOGGER.info("LA CAMPAÑA "+campana);
        }catch (SQLException e){
            LOGGER.error(e.getMessage());
            throw new BackendException("no se pudo preparar el query para Postgres");
        }
    }
    public static BorrarCampana conNombre(String nombre){
        return new BorrarCampana(nombre);
    }
}
