package com.solati.agentesvirtuales.interactions;

import com.solati.agentesvirtuales.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EliminarSesion implements Interaction {
    private static final Logger LOGGER = Logger.getLogger(BorrarCampana.class.getName());
    private String usuario;

    public EliminarSesion(String usuario){
        this.usuario=usuario;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        final Connection connection = actor.recall("ConexionBDMySQL");
        try{
            PreparedStatement preparedStatement = connection.prepareStatement("delete from controlsesion where usuario=?");
            preparedStatement.setObject(1, usuario);
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            LOGGER.error(e.getMessage());
            throw new BackendException("no se pudo preparar el query para mySQL");
        }
    }
    public static EliminarSesion aUsuario(String usuario){
        return new EliminarSesion(usuario);
    }
}
