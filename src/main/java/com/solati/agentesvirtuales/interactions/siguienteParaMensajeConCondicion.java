package com.solati.agentesvirtuales.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class siguienteParaMensajeConCondicion implements Interaction {
    Target Boton;
    Integer segundos;
    public siguienteParaMensajeConCondicion(Target boton){
        this.Boton=boton;
    }

    public static siguienteParaMensajeConCondicion siguiente(Target boton){
        return new siguienteParaMensajeConCondicion(boton);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(JavaScriptClick.on(Boton));
        WebDriverWait wait = new WebDriverWait(BrowseTheWeb.as(actor).getDriver(),120);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        System.out.println(alert.getText());
        alert.accept();
        WebDriverWait wait2 = new WebDriverWait(BrowseTheWeb.as(actor).getDriver(),120);
        Alert alert2 = wait.until(ExpectedConditions.alertIsPresent());
        actor.remember("ULTIMOMENSAJE", alert2.getText());
        System.out.println(alert2.getText());
        alert2.accept();
    }
}
