package com.solati.agentesvirtuales.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Step;

import static com.solati.agentesvirtuales.model.builder.ConnectionBuilder.crearNuevaConexion;

public class CrearConexionBDMySQL implements Interaction {

    private String userName;
    private String password;
    private String url;

    public CrearConexionBDMySQL(String url) {
        this.url = url;
    }

    public CrearConexionBDMySQL conUsuario(String userName) {
        this.userName = userName;
        return this;
    }

    public CrearConexionBDMySQL yClave(String password) {
        this.password = password;
        return this;
    }

    public static CrearConexionBDMySQL aBaseDeDatos(String url) {
        return Tasks.instrumented(CrearConexionBDMySQL.class, url);
    }

    @Step("{0} crea nueva conexion a #url con usuario:#userName y clave:#password")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.remember("ConexionBDMySQL",crearNuevaConexion(url).conUsuario(userName).yClave(password).build());
    }
}
