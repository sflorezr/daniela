package com.solati.agentesvirtuales.utils.hooks;

import com.solati.agentesvirtuales.abilities.ConsultaAbaseDatos;
import com.solati.agentesvirtuales.abilities.ConsultaAbaseDatosMySQL;
import net.serenitybdd.screenplay.actors.OnlineCast;
import cucumber.api.java.Before;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class ConsultarBaseDeDatos {
    @Before
    public void setup(){
        setTheStage(new OnlineCast());
        theActorCalled("robot").whoCan(ConsultaAbaseDatos.postGres());
        //theActorInTheSpotlight().whoCan(ConsultaAbaseDatosMySQL.mySQl());
    }
}
