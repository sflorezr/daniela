package com.solati.agentesvirtuales.utils;
import net.serenitybdd.core.Serenity;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface TomaEvidencia {

    static void deBaseDeDatos(ResultSet evidencia) throws SQLException {
        int MetaDataRS = 0;
        boolean bandera = true;
        String cadena="";
        MetaDataRS=evidencia.getMetaData().getColumnCount();

        while (evidencia.next()){
            System.out.println(evidencia.getMetaData().getColumnName(1));
        if(bandera==true){
            for (int i = 1; i <=evidencia.getMetaData().getColumnCount(); i++) {
                if(evidencia.getMetaData().getColumnDisplaySize(i)<80){
                    cadena=cadena+" | "+String.format("%-"+evidencia.getMetaData().getColumnDisplaySize(i)+"s", evidencia.getMetaData().getColumnName(i));
                }else
                {
                    cadena=cadena+" | "+String.format("%-"+30+"s", evidencia.getMetaData().getColumnName(i));
                }
            }
            cadena=cadena+"|"+"\n";
            bandera=false;
        }
            for (int i = 1; i <=evidencia.getMetaData().getColumnCount(); i++) {

             //   if (evidencia.getString(i)!=null && evidencia.getString(i).length()>0) {
                if(evidencia.getMetaData().getColumnDisplaySize(i)<80){
                    cadena=cadena+" | "+String.format("%-"+evidencia.getMetaData().getColumnDisplaySize(i)+"s", evidencia.getString(i));
                }else
                {
                    cadena=cadena+" | "+String.format("%-"+30+"s", evidencia.getString(i));
                }
            }
            cadena=cadena+"|"+"\n";
        }
        if (cadena!=""){
            Serenity.recordReportData().withTitle("Evidencia de base de datos ").andContents(cadena);
        }else{
            Serenity.recordReportData().withTitle("Evidencia de base de datos ").andContents("consulta vacia");
        }
    }
}
