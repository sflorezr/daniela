package com.solati.agentesvirtuales.utils.exceptions;

public class ActorNoPuedeConsultarLaBD extends RuntimeException {
    public ActorNoPuedeConsultarLaBD(String nombreActor){
        super("El actor "+nombreActor+" no tiene la habilidad de consultar en la BD");
    }
}
