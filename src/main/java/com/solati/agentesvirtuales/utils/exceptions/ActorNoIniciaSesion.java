package com.solati.agentesvirtuales.utils.exceptions;

public class ActorNoIniciaSesion extends RuntimeException  {
    public ActorNoIniciaSesion(String actorName){
        super("El actor "+ actorName + "no logra iniciar sesion");
    }
}
