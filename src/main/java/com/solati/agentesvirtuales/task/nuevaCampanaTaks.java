package com.solati.agentesvirtuales.task;

import com.ibm.icu.text.SimpleDateFormat;
import com.solati.agentesvirtuales.interactions.siguienteParaMensaje;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;

public class nuevaCampanaTaks implements Task {
    
    private String nombre;
    private String fecha_ini;
    private String fecha_fin;
    private String plazo;
    private String envio_a;
    private String tipoenvio;
    private String entidad;
    private String tipocampana;
    private String codig_exitoso;
    private String codig_fallido;
    private String horainicio;
    private String estado;
    private String carga_desde;
    private String flash;
    private String vmes="";
    public nuevaCampanaTaks(String nombre,String fecha_ini,String fecha_fin,String plazo,String envio_a,String tipoenvio,String entidad,String tipocampana,String codig_exitoso,String codig_fallido,String horainicio, String estado,String carga_desde,String flash){
        this.nombre=nombre;
        this.fecha_ini=fecha_ini;
        this.fecha_fin=fecha_fin;
        this.plazo=plazo;
        this.envio_a=envio_a;
        this.tipoenvio=tipoenvio;
        this.entidad=entidad;
        this.tipocampana=tipocampana;
        this.codig_exitoso=codig_exitoso;
        this.codig_fallido=codig_fallido;
        this.horainicio=horainicio;
        this.estado=estado;
        this.carga_desde=carga_desde;
        this.flash=flash;
    }

    public static nuevaCampanaTaks conDatos(String nombre,String fecha_ini,String fecha_fin,String plazo,String envio_a,String tipoenvio,String entidad,String tipocampana,String codig_exitoso,String codig_fallido,String horainicio, String estado,String carga_desde,String flash){
        return instrumented(nuevaCampanaTaks.class,nombre,fecha_ini,fecha_fin,plazo,envio_a,tipoenvio,entidad,tipocampana,codig_exitoso, codig_fallido, horainicio, estado, carga_desde,flash);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(TXT_NOMBRE,isClickable()).forNoMoreThan(10).seconds()
                ,Enter.theValue(nombre).into(TXT_NOMBRE));
        if (!fecha_ini.equals("")){
            theActorInTheSpotlight().attemptsTo(Click.on(TXT_FECHA_INI));

            /*theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(retornaMes(fecha_ini)).from(DATE_PICKER_INI_MES));
            theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(fecha_ini.substring(0,4)).from(DATE_PICKER_INI_ANO));
            theActorInTheSpotlight().attemptsTo(Click.on(DATE_PICKER_INI_DIA.of(retornaSemana(fecha_ini),retornaDia(fecha_ini))));
            retornaSemana(fecha_ini);*/
            theActorInTheSpotlight().attemptsTo(Hit.the(Keys.ENTER).into(TXT_FECHA_INI));
        }
        if (!fecha_fin.equals("")){
            theActorInTheSpotlight().attemptsTo(Click.on(TXT_FECHA_FIN));
            /*theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(retornaMes(fecha_ini)).from(DATE_PICKER_FIN_MES));
            theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(fecha_fin.substring(0,4)).from(DATE_PICKER_FIN_ANO));
            theActorInTheSpotlight().attemptsTo(Click.on(DATE_PICKER_INI_DIA.of(retornaSemana(fecha_fin),retornaDia(fecha_fin))));*/
            theActorInTheSpotlight().attemptsTo(Hit.the(Keys.ENTER).into(TXT_FECHA_FIN));
        }
        theActorInTheSpotlight().attemptsTo(Enter.theValue(plazo).into(TXT_PLAZO));
        if (!envio_a.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(envio_a).from(TBL_ENVIO_A)); }
        if (!tipoenvio.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(tipoenvio).from(TBL_TIPO_ENVIO));}
        /*if (!entidad.equals("")) {theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(entidad).from(TBL_ENTIDAD));}*/
        if (!tipocampana.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(tipocampana).from(TBL_TIPO_CAMPANA ));}
        if (!codig_exitoso.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(codig_exitoso).from(TBL_COD_GES_EXITO));}
        if (!horainicio.equals("")) {theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(horainicio).from(TBL_HORA_INICIO));}
        theActorInTheSpotlight().attemptsTo(Click.on(CHECK_MARTES));
        theActorInTheSpotlight().attemptsTo(Click.on(CHECK_MIERCOLES));
        theActorInTheSpotlight().attemptsTo(Click.on(CHECK_JUEVES));
        theActorInTheSpotlight().attemptsTo(Click.on(CHECK_VIERNES));
        /*theActorInTheSpotlight().attemptsTo(Click.on(CHECK_SABADO));
        theActorInTheSpotlight().attemptsTo(Click.on(CHECK_DOMINGO));*/
        if (!estado.equals("")) {theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(estado).from(TBL_ESTADO));
        }
        if(codig_exitoso.equals("")){theActorInTheSpotlight().attemptsTo(siguienteParaMensaje.siguiente(BOTON_SIGUIENTE));
        }else{theActorInTheSpotlight().attemptsTo(Click.on(BOTON_SIGUIENTE));}

    }

    public String retornaMes(String mes){
        String valMes="";
        switch (mes.substring(5,7)){
            case "01":valMes = "Ene";break;
            case "02":valMes = "Feb";break;
            case "03":valMes = "Mar";break;
            case "04":valMes = "Abr";break;
            case "05":valMes = "May";break;
            case "06":valMes = "Jun";break;
            case "07":valMes = "Jul";break;
            case "08":valMes = "Ago";break;
            case "09":valMes = "Sep";break;
            case "10":valMes = "Oct";break;
            case "11":valMes = "Nov";break;
            case "12":valMes = "Dic";break;
        }
        return valMes;
    }

    public String retornaSemana(String fecha) {
        String semana = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaActual = null;
        try {
            fechaActual = df.parse(fecha);
        } catch (ParseException e) {
            System.err.println("No se ha podido parsear la fecha.");
            e.printStackTrace();
        }
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaActual);
        int diaSemana = fechaCalendario.get(Calendar.WEEK_OF_MONTH);
        semana = String.valueOf(diaSemana+1);
        return semana;
    }
    public String retornaDia(String date){
        String fecha=date;
        String dia = "";
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        Date fechaActual = null;
        try {
            fechaActual = df.parse(fecha);
        } catch (ParseException e) {
            System.err.println("No se ha podido parsear la fecha.");
            e.printStackTrace();
        }
        GregorianCalendar fechaCalendario = new GregorianCalendar();
        fechaCalendario.setTime(fechaActual);
        int diaSemana = fechaCalendario.get(Calendar.WEEK_OF_MONTH);
        diaSemana = fechaCalendario.get(Calendar.DAY_OF_WEEK);
        dia = String.valueOf(diaSemana);
        int dia_devolver;
        if (dia.equals("1")){
            dia_devolver=7;
        }else{
            dia_devolver=Integer.parseInt(dia)-1;
        }
        return String.valueOf(dia_devolver);
    }
}
