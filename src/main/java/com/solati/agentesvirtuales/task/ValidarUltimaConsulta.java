package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.Consultar;
import com.solati.agentesvirtuales.questions.UltimaConsultaBD;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.Matchers.isIn;
import static java.util.Arrays.asList;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class ValidarUltimaConsulta implements Task {
    private List<String> campos;
    private List<String> items;

    public ValidarUltimaConsulta tieneLosItems(String... items){
        this.items=asList(items);
        return this;
    }

    public ValidarUltimaConsulta(List<String> campos){
        this.campos=campos;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        campos.forEach(campo->System.out.println(UltimaConsultaBD.consulta(campo).answeredBy(actor)));
        campos.forEach(campo->actor.should(seeThat(UltimaConsultaBD.consulta(campo),isIn(items))));
/*
        for (int i=0;i<campos.size();i++){
            System.out.println("titulo: "+campos.get(i));
            System.out.println("campo a validar: "+UltimaConsultaBD.consulta(campos.get(i)));
           // System.out.println(items);
            actor.should(seeThat(UltimaConsultaBD.consulta(campos.get(i)),isIn(items)));
        }
 */
    }
    public static ValidarUltimaConsulta conCampos(String... campos){
        try{
            Consultar.obtenerUltimaRespuesta().next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ValidarUltimaConsulta(asList(campos));
    }
}
