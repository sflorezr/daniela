package com.solati.agentesvirtuales.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.solati.agentesvirtuales.pageObjests.aprobarCampanaPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class menuGestionarCampanaTask implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_MENU_AGENTES,isVisible()).forNoMoreThan(10).seconds(),Click.on(BTN_MENU_AGENTES));
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_SUB_MENU_GESTION_CAMPANA,isVisible()).forNoMoreThan(10).seconds(),Click.on(BTN_SUB_MENU_GESTION_CAMPANA));
    }
    public static menuGestionarCampanaTask isaGestionarCapana(){
        return instrumented(menuGestionarCampanaTask.class);
    }
}
