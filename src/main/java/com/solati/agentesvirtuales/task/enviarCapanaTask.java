package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.siguienteParaMensajeConCondicion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static com.solati.agentesvirtuales.pageObjests.aprobarCampanaPage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class enviarCapanaTask implements Task {
    private String campana;
    public enviarCapanaTask(String campana){
        this.campana=campana;
    }
    public static enviarCapanaTask Capana(String campana){
        return instrumented(enviarCapanaTask.class,campana);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_ENVIAR.of(campana),isVisible()).forNoMoreThan(5).seconds());
        theActorInTheSpotlight().attemptsTo(siguienteParaMensajeConCondicion.siguiente(BTN_ENVIAR.of(campana)));
    }
}
