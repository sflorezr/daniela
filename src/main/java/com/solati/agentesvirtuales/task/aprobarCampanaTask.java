package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.siguienteParaMensajeConCondicion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.solati.agentesvirtuales.pageObjests.aprobarCampanaPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class aprobarCampanaTask implements Task {
    private String capana;
    public aprobarCampanaTask(String capana){
        this.capana=capana;
    }
    public static aprobarCampanaTask campanaTask(String capana){
        return instrumented(aprobarCampanaTask.class,capana);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_DETALLE.of(capana),isVisible()).forNoMoreThan(10).seconds(),Click.on(BTN_DETALLE.of(capana)));
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(LBL_NUMERO_REGISTROS,isVisible()).forNoMoreThan(600).seconds());
        actor.remember("cantidadaprobada",LBL_NUMERO_REGISTROS.resolveFor(actor).getTextValue());
        System.out.println(LBL_NUMERO_REGISTROS.resolveFor(actor).getTextValue());
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_APROBAR_CAMPANAS,isVisible()).forNoMoreThan(5).seconds(), siguienteParaMensajeConCondicion.siguiente(BTN_APROBAR_CAMPANAS));
    }
}
