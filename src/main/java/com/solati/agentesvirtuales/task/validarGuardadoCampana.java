package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.siguienteParaMensaje;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.BTN_GUARDAR;
import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.LBL_CANTIDAD_REGISTROS;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class validarGuardadoCampana implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        //theActorInTheSpotlight().attemptsTo();

        try {
            InputStream input = new FileInputStream("src/test/resources/temporales.properties");
            Properties prop = new Properties();
            prop.load(input);
            FileOutputStream output = new FileOutputStream("src/test/resources/temporales.properties");
            prop.setProperty(actor.recall("ultimonombre"),LBL_CANTIDAD_REGISTROS.resolveFor(actor).getText().split(":")[1].trim());
            prop.store(output,null);
            output.close();
            input.close();
            for (Enumeration e = prop.keys(); e.hasMoreElements(); ) {
                Object obj = e.nextElement();
                System.out.println(obj + ": "
                        + prop.getProperty(obj.toString()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*prop.put(actor.recall("ultimonombre"),LBL_CANTIDAD_REGISTROS.resolveFor(actor).getText().split(":")[1].trim());
        try {
           // prop.store(new FileOutputStream("src/test/resources/temporales.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        theActorInTheSpotlight().attemptsTo(siguienteParaMensaje.siguiente(BTN_GUARDAR));

    }
    public static validarGuardadoCampana validarGuardadoCampana(){
        return instrumented(validarGuardadoCampana.class);
    }
}
