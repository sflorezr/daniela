package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.siguienteParaMensajeConCondicion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static com.solati.agentesvirtuales.pageObjests.aprobarCampanaPage.BTN_APROBAR_CAMPANA;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class iraAprobarCampanaTask implements Task {
    @Override
    @Step("{0} ir hasta aprobar campañas")
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BOTON_MANTENIMIENTO,isVisible()).forNoMoreThan(5).seconds(), Click.on(BOTON_MANTENIMIENTO)
                ,WaitUntil.the(BOTON_CONTENEDOR,isVisible()).forNoMoreThan(5).seconds(),Click.on(BOTON_CONTENEDOR));
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BTN_APROBAR_CAMPANA,isVisible()).forNoMoreThan(5).seconds(), Click.on(BTN_APROBAR_CAMPANA));
    }
    public static iraAprobarCampanaTask iraprobarCampana(){
        return instrumented(iraAprobarCampanaTask.class);
    }
}
