package com.solati.agentesvirtuales.task;

import net.serenitybdd.markers.IsSilent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
public class cerrarSesionTask implements Task, IsSilent {
    public static cerrarSesionTask cerrarSesion(){return instrumented(cerrarSesionTask.class);}
    @Override
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(JavaScriptClick.on(BOTON_CERRAR));
        BrowseTheWeb.as(actor).getDriver().switchTo().alert().accept();
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BOTON_ENTRAR,isVisible()).forNoMoreThan(50).seconds());
    }
}
