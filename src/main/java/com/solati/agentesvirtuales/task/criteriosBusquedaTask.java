package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.siguienteParaMensaje;
import com.solati.agentesvirtuales.questions.mensajeAlertaQuestion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import static com.solati.agentesvirtuales.pageObjests.criteriosBusquedaPage.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class criteriosBusquedaTask implements Task {

    private String columna="";
    private String operador="";
    private String valor="";
    private String mensaje="";

    public criteriosBusquedaTask(String columna,String operador,String valor,String mensaje){
        this.columna=columna;
        this.operador=operador;
        this.valor=valor;
        this.mensaje=mensaje;
    }

    public static criteriosBusquedaTask conDatos(String columna,String operador,String valor,String mensaje){
        return instrumented(criteriosBusquedaTask.class,columna,operador,valor,mensaje);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        if(!columna.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(columna).from(TBL_COLUMNA)); }
        if(!operador.equals("")){theActorInTheSpotlight().attemptsTo(SelectFromOptions.byVisibleText(operador).from(TBL_OPERADOR)); }
        theActorInTheSpotlight().attemptsTo(Enter.theValue(valor).into(TXT_CAMPO));
        if(valor.equals("")){
            theActorInTheSpotlight().attemptsTo(siguienteParaMensaje.siguiente(BTN_AGREGAT));
            theActorInTheSpotlight().should(seeThat("mensaje de alerta", mensajeAlertaQuestion.mensaje(),equalTo(mensaje)));
        }else {
            theActorInTheSpotlight().attemptsTo(Click.on(BTN_AGREGAT));
            theActorInTheSpotlight().attemptsTo(Click.on(BTN_SIGUIENTE));
        }
    }
}
