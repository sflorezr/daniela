package com.solati.agentesvirtuales.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import java.time.LocalDate;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class gestionarCampanasTask implements Task {

    public gestionarCampanasTask(){
    }

    public static  gestionarCampanasTask iraGestionCapana(){
        return instrumented(gestionarCampanasTask.class);
    }

    @Override
    @Step("{0} ir hasta gestionar campañas")
    public <T extends Actor> void performAs(T actor) {
        theActorInTheSpotlight().attemptsTo(WaitUntil.the(BOTON_MANTENIMIENTO,isVisible()).forNoMoreThan(5).seconds(),Click.on(BOTON_MANTENIMIENTO)
                ,WaitUntil.the(BOTON_CONTENEDOR,isVisible()).forNoMoreThan(5).seconds(),Click.on(BOTON_CONTENEDOR)
                ,WaitUntil.the(BOTON_GESTION_CAMPANA,isVisible()).forNoMoreThan(10).seconds()
                , Click.on(BOTON_GESTION_CAMPANA)
                ,WaitUntil.the(BOTON_NUEVA_CAMPANA,isVisible()).forNoMoreThan(30).seconds()
                ,Click.on(BOTON_NUEVA_CAMPANA));
    }
}
