package com.solati.agentesvirtuales.task;

import com.solati.agentesvirtuales.interactions.Refrescar;
import com.solati.agentesvirtuales.pageObjests.agentesExternosPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.SilentTasks;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Base64;

import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static java.time.temporal.ChronoUnit.SECONDS;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class iniciarSesionTaks extends SilentTasks implements Task {
    private String usuario="";
    private String password="";

    public iniciarSesionTaks (String usuario,String password){
        this.usuario=usuario;
        byte[] decodedBytes = Base64.getDecoder().decode(password);
        this.password= new String(decodedBytes);
    }

    public static iniciarSesionTaks abrirAplicativo(String usuario, String password){
        setTheStage(new OnlineCast());
        return instrumented(iniciarSesionTaks.class,usuario,password);
    }

    @Override
    @Step("{0} Inicia abre aplicativo Adminfo y se loguea")
    public <T extends Actor> void performAs(T actor) {
        //theActorCalled("robot").attemptsTo(Open.browserOn().the(agentesExternosPage.class));
        //theActorCalled("robot").attemptsTo(Open.url(System.getProperty("ambiente")));
     //   theActorCalled("robot").attemptsTo(Open.url("https://www.google.com/"));
        
        theActorCalled("robot").attemptsTo(Open.url("https://qa.solati.corp/vsmart/"));
       // theActorInTheSpotlight().attemptsTo(Refrescar.navegadorActivo());
        theActorInTheSpotlight().attemptsTo(Click.on(BOTON_CONFIGURACION));
        theActorInTheSpotlight().attemptsTo(Click.on(BOTON_ACCEDER));
                theActorInTheSpotlight().attemptsTo(Enter.theValue(usuario).into(TXT_USUIARIO));
                theActorInTheSpotlight().attemptsTo( Enter.theValue(password).into(TXT_PASSWORD));
                theActorInTheSpotlight().attemptsTo(Click.on(BOTON_ENTRAR));
           //theActorInTheSpotlight().attemptsTo(Click.on(BOTON_CERRAR));

    }
}
