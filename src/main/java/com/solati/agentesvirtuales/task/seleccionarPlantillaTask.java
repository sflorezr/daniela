package com.solati.agentesvirtuales.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.solati.agentesvirtuales.pageObjests.agentesExternosPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class seleccionarPlantillaTask implements Task {
    private String plantilla;
    private String numerooculto;

    public seleccionarPlantillaTask(String plantilla,String numerooculto){
        this.plantilla=plantilla;
        this.numerooculto=numerooculto;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        if(!plantilla.equals("")){
            theActorInTheSpotlight().attemptsTo(WaitUntil.the(TBL_PLANTILLA,isVisible()).forNoMoreThan(120).seconds()
                    ,SelectFromOptions.byVisibleText(plantilla).from(TBL_PLANTILLA));
            theActorInTheSpotlight().attemptsTo(Enter.theValue(numerooculto).into(TXT_NUMOCULTO));

        }
    }

    public static seleccionarPlantillaTask conPlantillayNumeroOculto(String plantilla,String numerooculto){
        return instrumented (seleccionarPlantillaTask.class,plantilla,numerooculto);
    }
}
