# language: es
  #Autor: sflorez@solati.com.co

Característica: Pruebas aprobacion de campañas, agentes virtuales verificar log


  Esquema del escenario: Escenario: Verificar logs
    Cuando Consulto cantidad de logsEnviados de la campana: "<campana>"
    Entonces Valido cantidad respectivamente guardad de la campana "<campana>"

    Ejemplos:
      | campana         | criterio                     |
   #   | prueba11        | Solo a deudor y Nit          |
    #  | Prueba_Email1   | Solo a deudor y Nit          |
    #  | Prueba_Email1.1 | Solo a deudor y Obligacion   |
    #  | Prueba_Email2   | Solo a codeudor y nit        |
    #  | Prueba_Email2.1 | Solo a codeudor y Obligacion |
    #  | Prueba_Email3   | a ambos y Nit                |
    #  | Prueba_Email3.1 | a ambos y Obligacion         |
      | Prueba_SMS1     | Solo a deudor  y Nit         |
      | Prueba_SMS1.1   | Solo a deudor  y Obligacion  |
      | Prueba_SMS2     | Solo a codeudor y Nit        |
      | Prueba_SMS2.1   | Solo a codeudor y Obligacion |
      | Prueba_SMS3     | a ambos  y Nit               |
      | Prueba_SMS3.1   | a ambos  y Obligacion        |