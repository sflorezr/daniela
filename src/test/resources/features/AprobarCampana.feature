# language: es
# Autor: sflorez@solati.com.co

Característica: Pruebas aprobacion de campañas, agentes virtuales


  Esquema del escenario: Escenario: Aprobar Campaña y verificar logs
    Dado Ingreso al aplicativo adminfo
    Cuando Ingreso al modulo de mantenimiento en la opcion aprobar campana
    Y Busco y apruebo la campaña a aprobar "<campana>"
    Entonces Valido registros de la aprobacion y mensaje "<mensaje>"
    Cuando Ingreso a gestionar campana
    Y Envio la campana "<campana>"
    Entonces Valido mensaje final "<mensajefinal>"

    Ejemplos:
      | campana         | criterio                     | mensaje                                     | mensajefinal                                          |
      | Prueba_Email1   | Solo a deudor y Nit          | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_Email1.1 | Solo a deudor y Obligacion   | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_Email2   | Solo a codeudor y nit        | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_Email2.1 | Solo a codeudor y Obligacion | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_Email3   | a ambos y Nit                | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_Email3.1 | a ambos y Obligacion         | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS1     | Solo a deudor  y Nit         | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS1.1   | Solo a deudor  y Obligacion  | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS2     | Solo a codeudor y Nit        | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS2.1   | Solo a codeudor y Obligacion | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS3     | a ambos  y Nit               | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |
      #| Prueba_SMS3.1   | a ambos  y Obligacion        | Campaña y plantilla aprobadas exitosamente. | La campaña se empezará a ejecutar en unos momentos... |