package com.solati.agentesvirtuales.stepDefinitions;

import com.solati.agentesvirtuales.interactions.*;
import com.solati.agentesvirtuales.questions.mensajeAlertaQuestion;
import com.solati.agentesvirtuales.task.*;
import cucumber.api.DataTable;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.io.*;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class GestionCampanaStepDefinition {
    InputStream input = getClass().getClassLoader().getResourceAsStream("datos.properties");
    Properties prop = new Properties();

    @Dado("^Ingreso al aplicativo adminfo$")
    public void ingresoAlAplicativoAdminfo() throws InterruptedException, IOException {
        setTheStage(new OnlineCast());
        prop.load(input);
        //Path path = Path.
       // OutputStream out = new FileInputStream ();
        //prop.store(out, "some comment");
      //  theActorCalled("robot").attemptsTo((CrearConexionBDMySQL.aBaseDeDatos(prop.getProperty("urlBDMySQL")).conUsuario(prop.getProperty("usuarioMySQL")).yClave(prop.getProperty("claveMySQL"))));
      //  theActorInTheSpotlight().attemptsTo(EliminarSesion.aUsuario(prop.getProperty("usuario")));
        //theActorCalled("robot").attemptsTo(CrearConexionBD.aBaseDeDatos(prop.getProperty("urlBD")).conUsuario("root").yClave("")
          //                                  , Consultar.conQuery("select * from administracionivr where consivr = ?").conParametros("144"));
        //theActorInTheSpotlight().attemptsTo(ValidarUltimaConsulta.conCampos("nomivr","tipocampana","fecha_ini","fecha_fin","horaejecucion","estado","numocultos","diasplazopagos","grabador").tieneLosItems("prueba","EMAIL","2020-03-12","2020-03-12","13:00","A","sflorez@solati.com.co","10","pruebatest"));
        theActorCalled("robot").attemptsTo(iniciarSesionTaks.abrirAplicativo(prop.getProperty("usuario"),prop.getProperty("password")));
       // theActorInTheSpotlight().attemptsTo(cerrarSesionTask.cerrarSesion());
    }


    @Cuando("^Ingreso al modulo de mantenimiento en la opcion gestion campana$")
    public void ingresoAlModuloDeMantenimientoEnLaOpcionGestionCampana() {
        theActorInTheSpotlight().attemptsTo(gestionarCampanasTask.iraGestionCapana());
    }

    @Cuando("^Digito los datos de informacion general nombre:\"([^\"]*)\" fecha inicial:\"([^\"]*)\" fecha final:\"([^\"]*)\" plazo: \"([^\"]*)\" envio a:\"([^\"]*)\" tipo envio:\"([^\"]*)\" entidad:\"([^\"]*)\" tipo campana:\"([^\"]*)\" codig_exitoso:\"([^\"]*)\" codigo fallido:\"([^\"]*)\" hora inicio:\"([^\"]*)\" estado:\"([^\"]*)\" carga desde:\"([^\"]*)\" flash:\"([^\"]*)\"$")
    public void digitoLosDatosDeInformacionGeneralNombreFechaInicialFechaFinalPlazoEnvioATipoEnvioEntidadTipoCampanaCodig_exitosoCodigoFallidoHoraInicioEstadoCargaDesdeFlash(String nombre,String fecha_ini,String fecha_fin,String plazo,String envio_a,String tipoenvio,String entidad, String tipocampana, String codig_exitoso, String codig_fallido, String horainicio, String estado, String carga_desde,String flash) {
        //theActorCalled("robot").attemptsTo(BorrarCampana.conNombre(nombre));
        theActorCalled("robot").attemptsTo(CrearConexionBD.aBaseDeDatos(prop.getProperty("urlBD")).conUsuario("root").yClave("")
                , BorrarCampana.conNombre(nombre));
        theActorInTheSpotlight().remember("ultimonombre",nombre);
        theActorInTheSpotlight().attemptsTo(nuevaCampanaTaks.conDatos(nombre,fecha_ini,fecha_fin,plazo,envio_a,tipoenvio,entidad,tipocampana,codig_exitoso, codig_fallido, horainicio, estado, carga_desde,flash));
    }
    @Cuando("^Digito criterios de busqueda \"([^\"]*)\"$")
    public void digitoCriteriosDeBusqueda(String mensaje,DataTable tabla) {
        if (mensaje.equals("")){
            List<Map<String, String>> list = tabla.asMaps(String.class, String.class);
            System.out.println(list.size());
            for (int i=0;i<list.size();i++){
                theActorInTheSpotlight().attemptsTo(criteriosBusquedaTask.conDatos(list.get(i).get("Columna"),list.get(i).get("operador"),list.get(i).get("valor"),list.get(i).get("mensaje")));
            }
        }
    }
    @Entonces("^Valido respuesta de pantalla informacion general \"([^\"]*)\"$")
    public void validoRespuestaDePantallaInformacionGeneral(String mensaje) {
        if (!mensaje.equals("")){
            theActorInTheSpotlight().should(seeThat("mensaje de alerta", mensajeAlertaQuestion.mensaje(),equalTo(mensaje)));
            theActorInTheSpotlight().attemptsTo(cerrarSesionTask.cerrarSesion());
        }
    }

    @Cuando("^Selecciono plantilla \"([^\"]*)\" Y Digito numero o correo oculto y guardo campaña \"([^\"]*)\"$")
    public void seleccionoPlantillaYDigitoNumeroOCorreoOcultoYGuardoCampaña(String plantilla, String numoculto) {
        theActorInTheSpotlight().attemptsTo(seleccionarPlantillaTask.conPlantillayNumeroOculto(plantilla,numoculto));
    }
    @Entonces("^Valido ultimo mensaje \"([^\"]*)\" con nombre \"([^\"]*)\"$")
    public void validoUltimoMensajeConNombre(String mensaje, String nombre) {
        String mensajen="\""+nombre+ "\" " +mensaje;
        theActorInTheSpotlight().attemptsTo(validarGuardadoCampana.validarGuardadoCampana());
        theActorInTheSpotlight().should(seeThat(mensajeAlertaQuestion.mensaje(),equalTo(mensajen)));
        System.out.println("si fue a cerrar sesion");
        theActorInTheSpotlight().attemptsTo(cerrarSesionTask.cerrarSesion());
    }

}
