package com.solati.agentesvirtuales.stepDefinitions;

import com.solati.agentesvirtuales.questions.mensajeAlertaQuestion;
import com.solati.agentesvirtuales.questions.ultimaCantidadQuestion;
import com.solati.agentesvirtuales.task.enviarCapanaTask;
import com.solati.agentesvirtuales.task.menuGestionarCampanaTask;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Entonces;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

import com.solati.agentesvirtuales.task.iraAprobarCampanaTask;
import com.solati.agentesvirtuales.task.aprobarCampanaTask;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class aprobarCapanasStepDefinitions {

    InputStream input = getClass().getClassLoader().getResourceAsStream("temporales.properties");
    Properties prop = new Properties();
    private String campana;

    @Cuando("^Ingreso al modulo de mantenimiento en la opcion aprobar campana$")
    public void ingresoAlModuloDeMantenimientoEnLaOpcionAprobarCampana() {
        theActorInTheSpotlight().attemptsTo(iraAprobarCampanaTask.iraprobarCampana());
    }


    @Cuando("^Busco y apruebo la campaña a aprobar \"([^\"]*)\"$")
    public void buscoYAprueboLaCampañaAAprobar(String campana) {
        this.campana=campana;
        theActorInTheSpotlight().attemptsTo(aprobarCampanaTask.campanaTask(campana));
    }
    @Entonces("^Valido registros de la aprobacion y mensaje \"([^\"]*)\"$")
    public void validoRegistrosDeLaAprobacionYMensaje(String mensaje) throws IOException {
        prop.load(input);
        theActorInTheSpotlight().should(seeThat(mensajeAlertaQuestion.mensaje(),equalTo(mensaje)));
        theActorInTheSpotlight().should(seeThat(ultimaCantidadQuestion.cantidad(),equalTo(prop.getProperty(this.campana))));
    }

    @Cuando("^Ingreso a gestionar campana$")
    public void ingresoAGestionarCampana() {
        theActorInTheSpotlight().attemptsTo(menuGestionarCampanaTask.isaGestionarCapana());
    }

    @Cuando("^Envio la campana \"([^\"]*)\"$")
    public void envioLaCampana(String campana) {
        theActorInTheSpotlight().attemptsTo(enviarCapanaTask.Capana(campana));
    }

    @Entonces("^Valido mensaje final \"([^\"]*)\"$")
    public void validoMensajeFinal(String mensaje) {
        theActorInTheSpotlight().should(seeThat(mensajeAlertaQuestion.mensaje(),equalTo(mensaje)));
    }
}
