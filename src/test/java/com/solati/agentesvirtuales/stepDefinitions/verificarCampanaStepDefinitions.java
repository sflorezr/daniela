package com.solati.agentesvirtuales.stepDefinitions;

import com.solati.agentesvirtuales.interactions.Consultar;
import com.solati.agentesvirtuales.interactions.CrearConexionBD;
import com.solati.agentesvirtuales.task.ValidarUltimaConsulta;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class verificarCampanaStepDefinitions {
    private InputStream input = getClass().getClassLoader().getResourceAsStream("datos.properties");
    private Properties prop = new Properties();
    private InputStream input2 = getClass().getClassLoader().getResourceAsStream("consultas.properties");
    private Properties prop2 = new Properties();
    private InputStream input3 = getClass().getClassLoader().getResourceAsStream("temporales.properties");
    private Properties prop3 = new Properties();

    @Cuando("^Consulto cantidad de logsEnviados de la campana: \"([^\"]*)\"$")
    public void consultoCantidadDeLogsEnviadosDeLaCampana(String campana) throws IOException {
        setTheStage(new OnlineCast());
        prop.load(input);
        prop2.load(input2);
        theActorCalled("robot").attemptsTo(CrearConexionBD.aBaseDeDatos(prop.getProperty("urlBD")).conUsuario("root").yClave("")
                , Consultar.conQuery(prop2.getProperty("cantidadLog")).conParametros(campana));
    }

    @Entonces("^Valido cantidad respectivamente guardad de la campana \"([^\"]*)\"$")
    public void validoCantidadRespectivamenteGuardadDeLaCampana(String campana) throws IOException {
        prop3.load(input3);
        theActorInTheSpotlight().attemptsTo(ValidarUltimaConsulta.conCampos("CANTIDAD").tieneLosItems(prop3.getProperty(campana)));
    }

}
