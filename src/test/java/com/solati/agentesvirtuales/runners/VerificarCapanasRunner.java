package com.solati.agentesvirtuales.runners;

import com.palantir.docker.compose.DockerComposeRule;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

import java.io.IOException;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src//test//resources//features//VerificarLogCampana.feature"
        //,tags= {"@Ejecutar"}
        ,glue = {"com.solati.agentesvirtuales.stepDefinitions","com.solati.agentesvirtuales.utils.hooks"}
        ,snippets = SnippetType.CAMELCASE)
public class VerificarCapanasRunner {
    /* @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder().file("src/test/resources/docker-compose.yml").build();


    @BeforeClass
    public static void EjecutarDocker() throws IOException, InterruptedException {
        //  docker.exec(DockerComposeExecOption.noOptions(),"web56", DockerComposeExecArgument.arguments("bash php -f /var/www/html/smart/index.php rtr=mensajeria ctr=Campanas acc=consume tipo=EMAIL cicloinfinito=false"));
       // System.out.println(docker.containers().ip());
    }*/
}