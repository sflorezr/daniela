package com.solati.agentesvirtuales.runners;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.execution.DockerComposeExecArgument;
import com.palantir.docker.compose.execution.DockerComposeExecOption;
import com.solati.agentesvirtuales.utils.ST_Cambiar_Logo;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

import java.io.IOException;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src//test//resources//features//AprobarCampana.feature"
        //,tags= {"@Ejecutarrr"}
        ,glue = {"com.solati.agentesvirtuales.stepDefinitions","com.solati.agentesvirtuales.utils.hooks"},
        snippets = SnippetType.CAMELCASE)
public class AprobarCampanasRunner {
    /* @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder().file("src/test/resources/docker-compose.yml").build();

*/
    static ST_Cambiar_Logo cambiarLogo;
    @AfterClass
    public static void finishTestExe(){
        cambiarLogo = new ST_Cambiar_Logo();
        cambiarLogo.changeLogo();
    }
 /*   @BeforeClass
    public static void EjecutarDocker() throws IOException, InterruptedException {
        //  docker.exec(DockerComposeExecOption.noOptions(),"web56", DockerComposeExecArgument.arguments("bash php -f /var/www/html/smart/index.php rtr=mensajeria ctr=Campanas acc=consume tipo=EMAIL cicloinfinito=false"));
    }*/
}