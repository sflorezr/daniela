package com.solati.agentesvirtuales.runners;

import com.palantir.docker.compose.DockerComposeRule;
import com.solati.agentesvirtuales.utils.Docker;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.ClassRule;
import org.junit.runner.RunWith;



@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src//test//resources//features//GestionCampana.feature"
        //,tags= {"@Ejecutar"}
        ,glue = {"com.solati.agentesvirtuales.stepDefinitions","com.solati.agentesvirtuales.utils.hooks"},
        snippets = SnippetType.CAMELCASE)
public class CrearCampanasRunner {
  /*   @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder().file("src/test/resources/docker-compose.yml").build();
*/

}